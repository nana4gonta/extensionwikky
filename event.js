/* Ctrl-Shift-t がショートカットキー */
var keys = [17, 16, 84];
var keyLists = [];

function getKeycode(event){
  var keycode = event.keyCode;
  keyLists.push(keycode);
  
  if(keyLists.length == keys.length) {
    if(checkKeycode(keys, keyLists)) {
      createInput();
    }
    keyLists.splice(0, keyLists.length);
  }
}

// ショートカットキーのためのキーイベントを取得
$('body').on('keydown', function(event){
  getKeycode(event);
});

/*
$('#extWikky').live('submit', function(event) {
  check_input();
  return false;
})
*/

// jsで生成するフォームに対するイベント
$(document).on('submit', '#extWikky', function(event){
  var value = checkInput();
  execCommand( createCommand(value) );
  return false;
});
