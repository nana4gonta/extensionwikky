function compareArray(a, b) {
  if(a.length == b.length) {
    for(var i=0; i<a.length; i++){
      if(a[i] != b[i]) { return false; }
    }
    return true;
  }
  else { return false; }
}

function checkKeycode(expect, actual){
  return compareArray(expect, actual);
}

function createInput(){
  var body = $('body');
  var input = ["<form action='#' id='extWikky'>",
               "<input name='command' type='text'>",
               "<input value='send' type='submit'>",
               "</form>"].join('\n');
  body.append(input);
}

function checkInput () {
  var value = $('input[name="command"]').val();
  return value;
}

function createCommand(value) {
  // p hoge
  // e fuga
  var command = value[0];
  var params = value.slice(2, value.length);

  var href = "http://www.os.cis.iwate-u.ac.jp/wikky/wikky.cgi";
  switch(command) {
    case 's':
      // サーチ
      href = href + "?c=search&search_type=regexp&search_for=both" +
                      "&display=both&display_order=time&searchkey=" + params;
      break;
    case 'p':
      // ページを開く
      // wikky.cgi?ページ名
      href = href + "?" + params;
      break;
    case 'e':
      // エディット
      href = href + "?c=editp;p=" + params;
      break;
  }
  return href;
}

function execCommand(command) {
  return location.href = command;
}

if(typeof window === 'undefined') {
  module.exports = {"compareArray": compareArray};
  //module.exports = createCommand;
}
else {
}


/*
$.ajax({
  type: "GET",
  url: "http://www.os.cis.iwate-u.ac.jp/wikky/wikky.cgi?c=pagelist",
  success: function(data) {
    html = $(data).html();
    console.log($('div.main li > a', html));
  }
});
*/