var buster, command;

if (typeof require === "function" && typeof module === "object") {
  buster = require('buster');
  command = require('../command');
}

buster.testCase("Command Test", {
  "compare Array is True": function() {
    assert(command.compareArray([1], [1]));
  },

  "compare Array is False": function() {
    refute(command.compareArray([1], [2]));
  }
});
